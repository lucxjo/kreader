<!--
SPDX-FileCopyrightText: 2024 Louis Hollingworth <louis@hollingworth.nl>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# KReader
An RSS/Atom reader in Rust and Kirigami. Not affiliated with KDE other than using
their framework.

This project currently does anything that it should. Most of the current tree
will be from me playing around with Rust-QT-Binding_Generator

## Contact
You can contact me in these places should you wish:
 - [![Chat on XMPP](https://img.shields.io/badge/chat-on_xmpp-232634?style=for-the-badge&logo=xmpp&logoColor=white&labelColor=303446)](xmpp:louis@hollingworth.nl)
 - [![Chat on Matrix](https://img.shields.io/badge/chat-on%20matrix-85c1dc?style=for-the-badge&logo=matrix&logoColor=white&labelColor=303446)](https://matrix.to/#/@ludoviko_:matrix.org)
 - [![Mastodon Follow](https://img.shields.io/mastodon/follow/109337353066993672?domain=https%3A%2F%2Fmasto.nu&style=for-the-badge&logo=mastodon&logoColor=white&labelColor=303446&color=ca9ee6)](https://masto.nu/@Ludoviko)
